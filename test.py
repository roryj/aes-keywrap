import binascii
from aes_keywrap import aes_wrap_key, aes_unwrap_key
KEK = binascii.unhexlify("000102030405060708090A0B0C0D0E0F")
#CIPHER = binascii.unhexlify("1FA68B0A8112B447AEF34BD8FB5A7B829D3E862371D2CFE5")
PLAIN = binascii.unhexlify("AC26D14535B0D44B534A26C2107A2CD9")


CIPHER = aes_wrap_key(KEK, PLAIN)
print (binascii.hexlify(CIPHER).upper())

# PLAIN
print (binascii.hexlify(aes_unwrap_key(KEK, CIPHER)).upper())